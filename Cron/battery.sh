#!/bin/sh
# */5 * * * * /usr/local/bin/battery.sh

LOWER_LIMIT=30
UPPER_LIMIT=80

_state=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "state" | awk '{ print $2 }')
_percentage=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "percentage" | awk '{ print $2 }' | sed s/'%'/''/g)


if [[ $_state = "charging" && $_percentage -ge $UPPER_LIMIT ]]; then
	# echo "Hey, enough charging!"
	notify-send -u normal -i battery-good-charging -a "Battery" -t 60000 "Hey, enough charging!" "Battery is at $_percentage%"
fi

if [[ $_state = "discharging" && $_percentage -le $LOWER_LIMIT ]]; then
	# echo "Hey, low power!"
	notify-send -u critical -i battery-caution -a "Battery" -t 60000 "Hey, low power!" "Battery is at $_percentage%"
fi
