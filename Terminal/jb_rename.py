#!/usr/bin/env python3
import os, re

app_list = os.popen('wmctrl -l | grep \'Jitterbit Studio\'').read()

machine_name = 'b450-pc'

app_list = app_list.split('\n')

for app in app_list:
	if 'Jitterbit Studio' in app:
		win_id, app_name = app.split(machine_name)
		win_id = win_id.split(' ', 1)[0]
		# old_app_name = app_name.strip()
		new_app_name = re.sub(r'\sJitterbit Studio\s(\d+.)+\-\s', '', app_name)

		os.popen('wmctrl -i -r {WinID} -N \'{AppName}\''.format(WinID = win_id, AppName = new_app_name))

		print('WinID: ' + win_id.strip())
		print('Old Name: ' + app_name.strip())
		print('New Name: ' + new_app_name.strip())
