# Scripts

Some of my util scripts

```bash
cd /tmp/
git clone git@gitlab.com:AndersonFontana/linux-utils.git
cd /tmp/linux-utils/
cat bashrc >> ~/.bashrc
cat bash_profile >> ~/.bash_profile
```

## Some cool things

```bash
mkdir -p ~/.local/bin/
curl https://cht.sh/:cht.sh > ~/.local/bin/cht.sh
chmod +x ~/.local/bin/cht.sh
```
