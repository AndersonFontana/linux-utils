
# custom lines ------------ BEGIN ------------

export PS1="\[\033[36m\][\[\033[m\]\[\033[34m\]\u@\h\[\033[m\] \[\033[32m\]\W\[\033[m\]\[\033[36m\]]\[\033[m\]$ "

# Set current folder name as terminal title
PROMPT_COMMAND='echo -en "\033]0; $(basename "$PWD")\a"'

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

alias mpv360="mpv --ytdl-format=18 $1"
alias weather="curl wttr.in/"
alias how="cht.sh"

# transfer.sh
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi 
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; echo "" >> $tmpfile; cat $tmpfile; rm -f $tmpfile; } 

vpn-cyberghost () {
	case $1 in
		"connect" ) sudo cyberghostvpn --traffic --country-code $2 --connect; ;;
		"stop"    ) sudo cyberghostvpn --stop; ;;
		"list"    )      cyberghostvpn --traffic --country-code; ;;
		"status"  ) sudo cyberghostvpn --status; ;;
		* ) echo "Invalid command. Please go to: https://support.cyberghostvpn.com/hc/en-us/articles/360020673194"; ;;
	esac
}


# User specific environment and startup programs

export JAVA_HOME="/usr/lib/jvm/jdk1.8.0_201"
export JRE_HOME="$JAVA_HOME/jre"
export ANDROID_HOME="/home/Anderson/Android/Sdk"
export GRADLE_HOME="/opt/gradle/gradle-5.6.2"
export PATH="$PATH:$JAVA_HOME/bin:$GRADLE_HOME/bin"


# custom lines ------------  END  ------------
