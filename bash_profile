
# custom lines ------------ BEGIN ------------

sed -i '/^poweroff/ d' ~/.bash_history
sed -i '/^reboot/ d' ~/.bash_history
sed -i '/^ls$/ d' ~/.bash_history
sed -i '/^ls -la$/ d' ~/.bash_history

# custom lines ------------  END  ------------
